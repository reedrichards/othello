use instant::Instant;
use std::{error, rc::Rc, vec};
use yew::prelude::*;

type CellRow = Vec<usize>;
type Cells = Vec<CellRow>;

// Change the alias to use `Box<dyn error::Error>`.
type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

// implment copy trait for universe so we can clone it
#[derive(Clone, PartialEq, Debug)]
struct Board {
    cells: Cells,
    turn: usize,
    turns: Vec<Cells>,
    player_1_pit: usize,
    player_2_pit: usize,
    last_turn: Option<(usize, usize)>,
}

impl Board {
    // rle input
    fn new() -> Board {
        let cells8x8: Cells = (0..2).map(|_| (0..6).map(|_| 4).collect()).collect();
        // set the initial state of the board
        // should be the center 4 cells
        // XO
        // OX

        let cells_copy = cells8x8
            .iter()
            .map(|row| row.iter().map(|cell| *cell).collect::<Vec<usize>>())
            .collect::<Vec<Vec<usize>>>();

        // convert grid to cells
        Board {
            cells: cells8x8,
            turn: 0,
            turns: vec![cells_copy],
            player_1_pit: 0,
            player_2_pit: 0,
            last_turn: None,
        }
    }

    fn legal_moves(&self) -> Vec<(usize, usize)> {
        match self.turn {
            0 => self.legal_moves_for_player(0),
            1 => self.legal_moves_for_player(1),
            _ => vec![],
        }
    }

    fn legal_moves_for_player(&self, player: usize) -> Vec<(usize, usize)> {
        let y = player;
        let mut legal_moves = vec![];
        for x in 0..6 {
            if self.cells[y][x] != 0 {
                legal_moves.push((y, x));
            }
        }
        legal_moves
    }

    fn calculate_score(&self, y: usize, x: usize, player: usize) -> f64 {
        // Clone the board and play the move
        let b = self.clone().play(x, y).unwrap();

        // Define heuristic weights
        let store_weight = 3.0;
        let pits_weight = 1.0;
        let future_move_weight = 1.0;

        // Calculate player-specific scores
        let (player_store, opponent_store, player_pits, opponent_pits) = match player {
            0 => (
                b.player_1_pit as f64,
                b.player_2_pit as f64,
                &b.cells[0],
                &b.cells[1],
            ),
            1 => (
                b.player_2_pit as f64,
                b.player_1_pit as f64,
                &b.cells[1],
                &b.cells[0],
            ),
            _ => (0.0, 0.0, &b.cells[0], &b.cells[1]), // Default case, though it should be unreachable
        };

        // Evaluation
        let store_score = store_weight * (player_store - opponent_store);
        let pits_score = pits_weight
            * (player_pits.iter().sum::<usize>() as f64
                - opponent_pits.iter().sum::<usize>() as f64);

        // Calculate future moves score
        let future_moves_score: f64 = player_pits
            .iter()
            .enumerate()
            .map(|(i, &stones)| {
                if stones + i == 6 {
                    future_move_weight
                } else {
                    0.0
                }
            })
            .sum();

        // Total score
        store_score + pits_score + future_moves_score
    }

    fn minimax(
        self,
        depth: usize,
        maximizing_player: usize,
        turn: usize,
        y: usize,
        x: usize,
        mut alpha: f64,
        mut beta: f64,
        durationFunc: &impl Fn() -> bool,
    ) -> ((usize, usize), f64) {
        // play move,
        // if on next move, the game is over, return a score based on the turn
        // if the turn player wins, return 1000000
        // if the turn player loses, return -1000000
        // if the game is a draw, return 0

        let bclone = self.clone().play(x, y).unwrap();
        let game_over = bclone.legal_moves().len() == 0;
        if game_over {
            let player_score = match turn {
                0 => {
                    if bclone.player_1_pit > bclone.player_2_pit {
                        1000000.0
                    } else if bclone.player_1_pit < bclone.player_2_pit {
                        -1000000.0
                    } else {
                        0.0
                    }
                }
                1 => {
                    if bclone.player_2_pit > bclone.player_1_pit {
                        1000000.0
                    } else if bclone.player_2_pit < bclone.player_1_pit {
                        -1000000.0
                    } else {
                        0.0
                    }
                }
                _ => 0.0,
            };
            return ((y, x), player_score);
        }

        let score = ((y, x), self.calculate_score(y, x, turn));
        // todo check for termination conditions
        if depth == 0 || durationFunc() {
            return score;
        }

        if maximizing_player == turn {
            let mut value = f64::NEG_INFINITY;
            let mut best_move: Option<(usize, usize)> = None;
            let legal_moves = self.legal_moves();
            for (y, x) in legal_moves {
                let b = self.clone().flip(y, x);

                let (_, child_score) = b.clone().minimax(
                    depth - 1,
                    maximizing_player,
                    b.clone().turn.clone(),
                    y,
                    x,
                    alpha,
                    beta,
                    durationFunc,
                );

                if child_score > value {
                    value = child_score;
                    best_move = Some((y, x));
                }
                if child_score > beta {
                    break;
                }
                alpha = alpha.max(value);
            }
            if Some((y, x)) == best_move {
                return ((y, x), value);
            }
        } else {
            let mut value = f64::INFINITY;
            let mut best_move: Option<(usize, usize)> = None;
            let legal_moves = self.legal_moves();
            for (y, x) in legal_moves {
                let b = self.clone().flip(y, x);
                let (_, child_score) = b.clone().minimax(
                    depth - 1,
                    maximizing_player,
                    b.clone().turn.clone(),
                    y,
                    x,
                    alpha,
                    beta,
                    durationFunc,
                );

                if child_score < value {
                    value = child_score;
                    best_move = Some((y, x));
                }
                if child_score < alpha {
                    break;
                }
                beta = beta.min(value);
            }
            if Some((y, x)) == best_move {
                return ((y, x), value);
            }
        }
        score
    }

    fn move_with_best_score(&self) -> (usize, usize) {
        let now = instant::Instant::now();

        let df = is_duration_from_now(now, 10);

        let legal_moves = self.legal_moves();
        let mut best_score = f64::NEG_INFINITY;
        let mut best_move: Option<(usize, usize)> = None;
        let moves_with_scores = legal_moves.iter().map(|(y, x)| {
            self.clone().minimax(
                32,
                self.turn,
                self.turn,
                *y,
                *x,
                f64::NEG_INFINITY,
                f64::INFINITY,
                &df,
            )
        });
        for ((y, x), score) in moves_with_scores {
            if score > best_score {
                best_score = score;
                best_move = Some((y, x));
            }
        }
        match best_move {
            Some((y, x)) => (y, x),
            _ => (0, 0),
        }
    }

    fn play(&mut self, x: usize, y: usize) -> Result<Self> {
        let mut b = self.flip(y, x);
        b.turns.push(b.cells.clone());
        b.last_turn = Some((y, x));
        Ok(b)
    }

    fn play_as_opponent(&mut self) -> Result<Self> {
        let (y, x) = self.move_with_best_score();
        self.play(x, y)
    }

    fn do_flip(self, cell: (usize, usize)) -> Self {
        let mut stones = self.cells[cell.0][cell.1];
        // distribute stones counter clockwise
        // when a stone is placesd out of bounds,
        // it should be placed in the pit
        let mut cells = self.cells.clone();
        let mut y = cell.0 as isize;
        let mut x = cell.1 as isize;
        let mut p1_pit = self.player_1_pit;
        let mut p2_pit = self.player_2_pit;
        cells[y as usize][x as usize] = 0;
        let mut last_turn_pit = false;
        match y {
            0 => {
                x -= 1;
            }
            1 => {
                x += 1;
            }
            _ => {}
        }
        while stones > 0 {
            match (y, x) {
                (0, -1) => {
                    p1_pit += 1;
                    stones -= 1;
                    y += 1;
                    x += 1;
                    if self.turn == 0 {
                        last_turn_pit = true;
                    }
                }
                (1, 6) => {
                    p2_pit += 1;
                    stones -= 1;
                    y -= 1;
                    x -= 1;
                    if self.turn == 1 {
                        last_turn_pit = true;
                    }
                }
                (0, _) => {
                    cells[y as usize][x as usize] += 1;
                    stones -= 1;
                    if stones == 0
                        && y == self.turn as isize
                        && cells[0][x as usize] == 1
                        && cells[1][x as usize] > 0
                    {
                        p1_pit += cells[1][x as usize] + 1;
                        cells[1][x as usize] = 0;
                        cells[0][x as usize] = 0;
                    }
                    x -= 1;
                    last_turn_pit = false;
                }
                (1, _) => {
                    cells[y as usize][x as usize] += 1;
                    stones -= 1;
                    if stones == 0
                        && y == self.turn as isize
                        && cells[1][x as usize] == 1
                        && cells[0][x as usize] > 0
                    {
                        p2_pit += cells[0][x as usize] + 1;
                        cells[1][x as usize] = 0;
                        cells[0][x as usize] = 0;
                    }
                    x += 1;
                    last_turn_pit = false;
                }
                (_, _) => {}
            }
            // check if last stone was placed across from an opponents stone
            // on my side of the board
            // if so, add the opponents stones to the pit and remove the opponents stones
            // from the board andy my stone
        }

        let turn = match (last_turn_pit, self.turn) {
            (true, 0) => 0,
            (true, 1) => 1,
            (false, 0) => 1,
            (false, 1) => 0,
            (_, _) => 0,
        };

        Board {
            cells,
            turn: turn,
            turns: self.turns,
            player_1_pit: p1_pit,
            player_2_pit: p2_pit,
            last_turn: None,
        }
    }

    fn flip(&mut self, y: usize, x: usize) -> Self {
        // search north south east west and diagonals for
        // a cell that is the opposite color and then flip,
        // if no cell is found then return error
        return self.clone().do_flip((y, x));
    }

    fn toggle_cell(self, y: usize, x: usize) -> Result<Self> {
        let mut new_board = self.clone();
        let mut b = new_board.flip(y, x);
        b.turns.push(b.cells.clone());
        b.last_turn = Some((y, x));
        Ok(b)
    }
}

/// impl Default for CounterState {
///     fn default() -> Self {
///         Self { counter: 1 }
///     }
/// }
///
impl Default for Board {
    fn default() -> Self {
        Self::new()
    }
}

// Define an enum for your actions instead of using function pointers.
enum BoardAction {
    ToggleCell(usize, usize),
    PrevTurn,
    PlayAsOpponent,
}

impl Reducible for Board {
    type Action = BoardAction;

    fn reduce(self: Rc<Self>, action: BoardAction) -> Rc<Self> {
        let mut new_board = (*self).clone();
        match action {
            BoardAction::ToggleCell(y, x) => {
                if let Ok(b) = new_board.clone().toggle_cell(y, x) {
                    new_board = b;
                }
            }
            BoardAction::PrevTurn => {
                if new_board.turns.len() > 1 {
                    new_board.turns.pop();
                    new_board.cells = new_board.turns[new_board.turns.len() - 1].clone();
                    new_board.turn = match new_board.turn {
                        0 => 1,
                        1 => 0,
                        _ => 0,
                    };
                }
            }
            BoardAction::PlayAsOpponent => {
                if let Ok(b) = new_board.clone().play_as_opponent() {
                    new_board = b;
                }
            }
        }

        Rc::new(new_board)
    }
}

#[function_component(Othello)]
pub fn othello() -> Html {
    let board = use_reducer(Board::default);

    html! {
        <div class="container mx-auto p-4" >
            <h1 class="text-3xl font-bold text-center mb-4">{ "Mancala" }</h1>



             <p class="italic">{" turn: "}{ match board.turn {
                0 => "player 1",
                1 => "player 2",
                _ => "player 1",
            } }</p>



                // render player 1 pit
                <div class="flex justify-center space-x-2">
                    <div class={format!("w-8 h-16 flex justify-center items-center rounded-full m-1 bg-black text-white {}",
                    {if board.turn == 0 {
                        "bg-green-500"
                    } else {
                        "bg-gray-500"
                    }})}>
                        { board.player_1_pit }
                    </div>
                    <div class="board max-w-md mx-auto">
                    <table class="table-fixed border-collapse border border-gray-400">
                        <tbody>
                            { for board.cells.iter().enumerate().map(|(y, row)|
                                html!(
                                    <tr key={format!("{}", y)} class="bg-green-600">
                                        { for row.iter().enumerate().map(|(x, cell)|
                                            html! (
                                                <td key={format!("{}{}", y, x)} class={format!(
                                                    "border border-black-300 {}",
                                                    { if board.last_turn == Some((y, x)) {
                                                        "bg-red-500"
                                                    } else {
                                                        ""
                                                    }}
                                                )}>
                                                    <div key={format!("{}{}", y, x)} class={"cell w-8 h-8 flex justify-center items-center rounded-full m-1 "}
                                                    onclick={
                                                        let board = board.clone();
                                                        Callback::from(move |_| {
                                                            board.dispatch(
                                                                BoardAction::ToggleCell(y, x),
                                                            )
                                                        })
                                                    }>
                                                        { cell }
                                                    </div>
                                                </td>
                                            ))
                                        }
                                    </tr>
                                )
                            ) }
                        </tbody>
                    </table>
                </div>
                    // <div class="w-8 h-16 flex justify-center items-center rounded-full m-1 bg-black text-white">
                    //     { board.player_2_pit }
                    // </div>
                    <div class={format!("w-8 h-16 flex justify-center items-center rounded-full m-1 text-white {}",
                    {if board.turn == 1 {
                        "bg-green-500"
                    } else {
                        "bg-gray-500"
                    }})}>
                        { board.player_2_pit }
                    </div>

                </div>



                <div class="flex justify-center mt-4 space-x-2">
                <button class="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-700 transition duration-300" onclick={
                    let board = board.clone();
                    Callback::from(move |_| {
                        board.dispatch(BoardAction::PlayAsOpponent);
                    })
                }>
                    { "AI Move" }
                </button>
                <button class="px-4 py-2 bg-green-500 text-white rounded hover:bg-green-700 transition duration-300" onclick={
                    let board = board.clone();
                    Callback::from(move |_| {
                        board.dispatch(BoardAction::PrevTurn)
                    })
                }>
                    { "Prev Turn" }
                </button>
            </div>

            </div>
    }
}

#[function_component(Mancala)]
pub fn app() -> Html {
    html! {
        <div>
            {

                    html! {
                        <div>
                            <Othello />
                        </div>
                    }

            }
        </div>
    }
}

fn is_duration_from_now(n: Instant, duration: u64) -> impl Fn() -> bool {
    move || n.elapsed().as_secs() > duration
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_legal_moves() {
        let cases = vec![
            (
                // initial board state
                vec![vec![4, 4, 4, 4, 4, 4], vec![4, 4, 4, 4, 4, 4]],
                0,
                // expected legal moves
                vec![(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5)],
                "legal moves for player 1",
            ),
            (
                // initial board state
                vec![vec![4, 4, 4, 4, 4, 4], vec![4, 4, 4, 4, 4, 4]],
                1,
                // expected legal moves
                vec![(1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5)],
                "legal moves for player 1",
            ),
        ];
        cases
            .iter()
            .for_each(|(initial_state, turn, expected_legal_moves, msg)| {
                let board = Board {
                    player_1_pit: 0,
                    player_2_pit: 0,
                    cells: initial_state.clone(),
                    turn: *turn as usize,
                    turns: vec![initial_state.clone()],
                    last_turn: None,
                };
                let legal_moves = board.legal_moves();
                assert_eq!(legal_moves, *expected_legal_moves, "{}", msg);
            });
    }

    #[test]
    fn test_do_flip() {
        let cases = vec![
            (
                // initial board state
                Board {
                    player_1_pit: 0,
                    player_2_pit: 0,
                    cells: vec![vec![0, 4, 4, 4, 4, 4], vec![4, 4, 4, 4, 4, 4]],
                    turn: 0,
                    turns: vec![],
                    last_turn: None,
                },
                // cell to flip
                (0, 4),
                // expected board state
                Board {
                    player_1_pit: 5,
                    player_2_pit: 0,
                    cells: vec![vec![0, 5, 5, 5, 0, 4], vec![0, 4, 4, 4, 4, 4]],
                    turn: 0,
                    turns: vec![vec![vec![0, 4, 4, 4, 4, 4], vec![4, 4, 4, 4, 4, 4]]],
                    last_turn: None,
                },
            ),
            // test the opposite way
            (
                // initial board state
                Board {
                    player_1_pit: 0,
                    player_2_pit: 0,
                    cells: vec![vec![4, 4, 4, 4, 4, 4], vec![4, 4, 4, 4, 4, 0]],
                    turn: 1,
                    turns: vec![],
                    last_turn: None,
                },
                // cell to flip
                (1, 1),
                // expected board state
                Board {
                    player_1_pit: 0,
                    player_2_pit: 5,
                    cells: vec![vec![4, 4, 4, 4, 4, 0], vec![4, 0, 5, 5, 5, 0]],
                    turn: 1,
                    turns: vec![vec![vec![0, 4, 4, 4, 4, 4], vec![4, 4, 4, 4, 4, 4]]],
                    last_turn: None,
                },
            ),
            (
                Board {
                    player_1_pit: 15,
                    player_2_pit: 13,
                    cells: vec![vec![0, 0, 0, 8, 12, 0], vec![2, 0, 0, 0, 0, 0]],
                    turn: 1,
                    turns: vec![],
                    last_turn: None,
                },
                (1, 0),
                Board {
                    player_1_pit: 15,
                    player_2_pit: 13,
                    cells: vec![vec![0, 0, 0, 8, 12, 0], vec![0, 1, 1, 0, 0, 0]],
                    turn: 1,
                    turns: vec![],
                    last_turn: None,
                },
            ),
        ];
        cases
            .iter()
            .for_each(|(initial_state, cell, expected_state)| {
                let board = initial_state.clone().do_flip(*cell);
                assert_eq!(board.cells, *expected_state.cells);
                assert_eq!(board.player_1_pit, expected_state.player_1_pit);
                assert_eq!(board.player_2_pit, expected_state.player_2_pit);
            });
    }
}
