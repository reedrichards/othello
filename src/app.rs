use instant::Instant;
use std::{error, rc::Rc, vec};
use yew::prelude::*;
use yew_router::prelude::*;

mod mancala;
use mancala::Mancala;
type CellRow = Vec<usize>;
type Cells = Vec<CellRow>;

// Change the alias to use `Box<dyn error::Error>`.
type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

// implment copy trait for universe so we can clone it
#[derive(Clone, PartialEq)]
struct Board {
    cells: Cells,
    turn: usize,
    turns: Vec<Cells>,
}

impl Board {
    // rle input
    fn new() -> Board {
        let mut cells8x8: Cells = (0..8).map(|_| (0..8).map(|_| 0).collect()).collect();
        // set the initial state of the board
        // should be the center 4 cells
        // XO
        // OX
        cells8x8[3][3] = 1;
        cells8x8[3][4] = 2;
        cells8x8[4][3] = 2;
        cells8x8[4][4] = 1;

        let cells_copy = cells8x8
            .iter()
            .map(|row| row.iter().map(|cell| *cell).collect::<Vec<usize>>())
            .collect::<Vec<Vec<usize>>>();

        // convert grid to cells
        Board {
            cells: cells8x8,
            turn: 0,
            turns: vec![cells_copy],
        }
    }

    fn pretty_print(&self) {
        self.cells.iter().enumerate().for_each(|(y, row)| {
            row.iter().enumerate().for_each(|(x, cell)| {
                print!(" {} ", cell);
            });
            println!();
        });
    }

    fn legal_moves(&self) -> Vec<(usize, usize)> {
        self.clone()
            .cells
            .iter()
            .enumerate()
            .map(|(y, row)| {
                row.iter().enumerate().fold(
                    vec![],
                    |mut acc: Vec<(usize, usize)>, (x, cell): (usize, &usize)| {
                        if *cell == 0 {
                            let (_, flipped) = self.clone().flip(y, x);
                            if flipped {
                                acc.push((y, x));
                            }
                        }
                        acc
                    },
                )
            })
            .flatten()
            .collect::<Vec<(usize, usize)>>()
    }

    fn calculate_score(&self, y: usize, x: usize, player: usize) -> f64 {
        let (player_score, opponent_score) = match player {
            0 => (
                self.cells.iter().enumerate().fold(0.0, |acc, (y, row)| {
                    acc + row.iter().enumerate().fold(0.0, |acc, (x, cell)| {
                        if *cell == 1 {
                            acc + calculate_weight(y, x)
                        } else {
                            acc
                        }
                    })
                }),
                104.0
                    - self.cells.iter().enumerate().fold(0.0, |acc, (y, row)| {
                        acc + row.iter().enumerate().fold(0.0, |acc, (x, cell)| {
                            if *cell == 2 {
                                acc + calculate_weight(y, x)
                            } else {
                                acc
                            }
                        })
                    }),
            ),
            _ => (
                self.cells.iter().enumerate().fold(0.0, |acc, (y, row)| {
                    acc + row.iter().enumerate().fold(0.0, |acc, (x, cell)| {
                        if *cell == 2 {
                            acc + calculate_weight(y, x)
                        } else {
                            acc
                        }
                    })
                }),
                104.0
                    - self.cells.iter().enumerate().fold(0.0, |acc, (y, row)| {
                        acc + row.iter().enumerate().fold(0.0, |acc, (x, cell)| {
                            if *cell == 1 {
                                acc + calculate_weight(y, x)
                            } else {
                                acc
                            }
                        })
                    }),
            ),
        };
        player_score
    }

    fn count_ones(&self) -> usize {
        self.cells
            .iter()
            .flatten()
            .filter(|&cell| *cell == 1)
            .count()
    }

    fn count_twos(&self) -> usize {
        self.cells
            .iter()
            .flatten()
            .filter(|&cell| *cell == 2)
            .count()
    }

    fn minimax(
        self,
        depth: usize,
        maximizing_player: usize,
        turn: usize,
        y: usize,
        x: usize,
        mut alpha: f64,
        mut beta: f64,
        durationFunc: &impl Fn() -> bool,
    ) -> ((usize, usize), f64) {
        let score = ((y, x), self.calculate_score(y, x, maximizing_player));
        // todo check for termination conditions
        if depth == 0 || durationFunc() {
            return score;
        }

        if maximizing_player == turn {
            let mut value = f64::NEG_INFINITY;
            let mut best_move: Option<(usize, usize)> = None;
            let legal_moves = self.legal_moves();
            for (y, x) in legal_moves {
                let (b, _) = self.clone().flip(y, x);

                let (_, child_score) = b.minimax(
                    depth - 1,
                    maximizing_player,
                    {
                        match turn {
                            0 => 1,
                            1 => 0,
                            _ => turn,
                        }
                    },
                    y,
                    x,
                    alpha,
                    beta,
                    durationFunc,
                );

                if child_score > value {
                    value = child_score;
                    best_move = Some((y, x));
                }
                if child_score > beta {
                    break;
                }
                alpha = alpha.max(value);
            }
            if Some((y, x)) == best_move {
                return ((y, x), value);
            }
        } else {
            let mut value = f64::INFINITY;
            let mut best_move: Option<(usize, usize)> = None;
            let legal_moves = self.legal_moves();
            for (y, x) in legal_moves {
                let (b, _) = self.clone().flip(y, x);
                let (_, child_score) = b.minimax(
                    depth - 1,
                    maximizing_player,
                    {
                        match turn {
                            0 => 1,
                            1 => 0,
                            _ => turn,
                        }
                    },
                    y,
                    x,
                    alpha,
                    beta,
                    durationFunc,
                );

                if child_score < value {
                    value = child_score;
                    best_move = Some((y, x));
                }
                if child_score < alpha {
                    break;
                }
                beta = beta.min(value);
            }
            if Some((y, x)) == best_move {
                return ((y, x), value);
            }
        }
        score
    }

    fn move_with_best_score(&self) -> (usize, usize) {
        // let ((y, x), _) = self.clone().minimax(3, true, self.turn, 0, 0);

        let now = instant::Instant::now();

        let df = is_duration_from_now(now, 5);

        let legal_moves = self.legal_moves();
        let mut best_score = f64::NEG_INFINITY;
        let mut best_move: Option<(usize, usize)> = None;
        let moves_with_scores = legal_moves.iter().map(|(y, x)| {
            self.clone().minimax(
                16,
                self.turn,
                self.turn,
                *y,
                *x,
                f64::NEG_INFINITY,
                f64::INFINITY,
                &df,
            )
        });
        for ((y, x), score) in moves_with_scores {
            if score > best_score {
                best_score = score;
                best_move = Some((y, x));
            }
        }
        match best_move {
            Some((y, x)) => (y, x),
            _ => (0, 0),
        }
    }

    fn play(&mut self, x: usize, y: usize) -> Result<Self> {
        let cell = self.cells[y][x];
        let turn = self.turn;
        let (mut b, flipped) = self.flip(y, x);
        if !flipped {
            self.cells[y][x] = cell;
            self.turn = turn;
            return Err("illegal move".into());
        }
        b.turn = match self.turn {
            0 => 1,
            1 => 0,
            _ => self.turn,
        };
        b.turns.push(b.cells.clone());
        Ok(b)
    }

    fn play_as_opponent(&mut self) -> Result<Self> {
        let (y, x) = self.move_with_best_score();
        self.play(x, y)
    }

    fn distance_to_flip(
        &self,
        cell: (usize, usize),
        dy: isize,
        dx: isize,
        inc: (isize, isize),
        color: usize,
    ) -> isize {
        let distance = 0;
        // check if next cell is not 0 and not the same color
        let curr_y = cell.0 as isize + dy;
        let curr_x = cell.1 as isize + dx;
        // bounds check
        if curr_y < 0 || curr_y > 7 {
            return 0;
        }

        if curr_x < 0 || curr_x > 7 {
            return 0;
        }

        let next_cell =
            self.cells[(cell.0 as isize + dy) as usize][(cell.1 as isize + dx) as usize];
        if next_cell != 0 && next_cell != color {
            return 1 + self.distance_to_flip(cell, dy + inc.0, dx + inc.1, inc, color);
        }
        distance
    }

    fn has_same_color(
        &self,
        cell: (usize, usize),
        dy: isize,
        dx: isize,
        inc: (isize, isize),
        color: usize,
    ) -> bool {
        let curr_y = cell.0 as isize + dy;
        let curr_x = cell.1 as isize + dx;
        // bounds check
        if curr_y < 0 || curr_y > 7 {
            return false;
        }

        if curr_x < 0 || curr_x > 7 {
            return false;
        }

        // if current cell is 0 return false
        if self.cells[curr_y as usize][curr_x as usize] == 0 {
            return false;
        }

        let next_cell =
            self.cells[(cell.0 as isize + dy) as usize][(cell.1 as isize + dx) as usize];
        if next_cell == color {
            return true;
        }
        self.has_same_color(cell, dy + inc.0, dx + inc.1, inc, color)
    }

    fn should_flip(
        &self,
        cell: (usize, usize),
        dy: isize,
        dx: isize,
        inc: (isize, isize),
        color: usize,
    ) -> bool {
        let distance = self.distance_to_flip(cell, dy, dx, (dy, dx), color);
        // search for a cell that is the same color
        let same_color = self.has_same_color(cell, dy, dx, inc, color);
        // println!("distance: {}, same_color: {}", distance, same_color);
        distance > 0 && same_color
    }

    fn do_flip(self, cell: (usize, usize)) -> (Self, bool) {
        let directions: [(isize, isize); 8] = [
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0),
            (1, 1),
            (1, -1),
            (-1, 1),
            (-1, -1),
        ];
        let mut flipped = false;
        let mut new_board = self.clone();
        let original_cell = self.cells[cell.0][cell.1];
        let color_to_set = match self.turn {
            0 => 1,
            _ => 2,
        };
        new_board.cells[cell.0][cell.1] = color_to_set;
        for (dy, dx) in directions.iter() {
            let distance_to_flip = self.distance_to_flip(cell, *dy, *dx, (*dy, *dx), color_to_set);
            let should_flip = self.should_flip(cell, *dy, *dx, (*dy, *dx), color_to_set);
            if should_flip {
                let mut current_y = cell.0 as isize + dy;
                let mut current_x = cell.1 as isize + dx;
                for _ in 0..distance_to_flip {
                    new_board.cells[current_y as usize][current_x as usize] = color_to_set;
                    current_y += dy;
                    current_x += dx;
                }
                flipped = true;
            }
        }

        if !flipped {
            new_board.cells[cell.0][cell.1] = original_cell;
        }

        (new_board, flipped)
    }

    fn flip(&mut self, y: usize, x: usize) -> (Self, bool) {
        // search north south east west and diagonals for
        // a cell that is the opposite color and then flip,
        // if no cell is found then return error
        return self.clone().do_flip((y, x));
    }

    fn toggle_cell(self, y: usize, x: usize) -> Result<Self> {
        let cell = self.cells[y][x];
        let turn = self.turn;
        let mut new_board = self.clone();
        let legal_moves = self.legal_moves();
        if !legal_moves.contains(&(y, x)) {
            return Err("illegal move".into());
        }
        let (mut b, flipped) = new_board.flip(y, x);
        if !flipped {
            new_board.cells[y][x] = cell;
            new_board.turn = turn;
        }
        b.turn = match self.turn {
            0 => 1,
            1 => 0,
            _ => self.turn,
        };
        b.turns.push(b.cells.clone());
        Ok(b)

        // b.clone().play_as_opponent()
    }
}

// https://play-othello.appspot.com/files/Othello.pdf
fn calculate_weight(y: usize, x: usize) -> f64 {
    let weight = vec![
        vec![120, -20, 20, 5, 5, 20, -20, 120],
        vec![-20, -40, -5, -5, -5, -5, -40, -20],
        vec![20, -5, 15, 3, 3, 15, -5, 20],
        vec![5, -5, 3, 3, 3, 3, -5, 5],
        vec![5, -5, 3, 3, 3, 3, -5, 5],
        vec![20, -5, 15, 3, 3, 15, -5, 20],
        vec![-20, -40, -5, -5, -5, -5, -40, -20],
        vec![120, -20, 20, 5, 5, 20, -20, 120],
    ];
    weight[y][x] as f64
}
/// impl Default for CounterState {
///     fn default() -> Self {
///         Self { counter: 1 }
///     }
/// }
///
impl Default for Board {
    fn default() -> Self {
        Self::new()
    }
}

// Define an enum for your actions instead of using function pointers.
enum BoardAction {
    ToggleCell(usize, usize),
    PrevTurn,
    PlayAsOpponent,
}

impl Reducible for Board {
    type Action = BoardAction;

    fn reduce(self: Rc<Self>, action: BoardAction) -> Rc<Self> {
        let mut new_board = (*self).clone();
        match action {
            BoardAction::ToggleCell(y, x) => {
                if let Ok(b) = new_board.clone().toggle_cell(y, x) {
                    new_board = b;
                }
            }
            BoardAction::PrevTurn => {
                if new_board.turns.len() > 1 {
                    new_board.turns.pop();
                    new_board.cells = new_board.turns[new_board.turns.len() - 1].clone();
                    new_board.turn = match new_board.turn {
                        0 => 1,
                        1 => 0,
                        _ => 0,
                    };
                }
            }
            BoardAction::PlayAsOpponent => {
                if let Ok(b) = new_board.clone().play_as_opponent() {
                    new_board = b;
                }
            }
        }

        Rc::new(new_board)
    }
}

#[function_component(Othello)]
pub fn othello() -> Html {
    let board = use_reducer(Board::default);

    html! {
        <div class="container mx-auto p-4" >
            <h1 class="text-3xl font-bold text-center mb-4">{ "Othello/Reversi" }</h1>
            <p class="font-semibold"> {" score black: "}{ board.count_ones() }</p>
            <p class="font-semibold">{" score white: "}{ board.count_twos() }</p>
            <p class="italic">{" turn: "}{ match board.turn {
                0 => "black",
                1 => "white",
                _ => "black",
            } }</p>




                <div class="board max-w-md mx-auto">
                    <table class="table-fixed border-collapse border border-gray-400">
                        <tbody>
                            { for board.cells.iter().enumerate().map(|(y, row)|
                                html!(
                                    <tr key={format!("{}", y)} class="bg-green-600">
                                        { for row.iter().enumerate().map(|(x, cell)|
                                            html! (
                                                <td key={format!("{}{}", y, x)} class="border border-gray-300">
                                                    <div key={format!("{}{}", y, x)} class={format!("cell w-8 h-8 flex justify-center items-center rounded-full m-1 {}", match cell {
                                                        0 => "bg-transparent",
                                                        1 => "bg-black",
                                                        _ => "bg-white",
                                                    })}
                                                    onclick={
                                                        let board = board.clone();
                                                        Callback::from(move |_| {
                                                            board.dispatch(
                                                                BoardAction::ToggleCell(y, x),
                                                            )
                                                        })
                                                    }>
                                                        { " " }
                                                    </div>
                                                </td>
                                            ))
                                        }
                                    </tr>
                                )
                            ) }
                        </tbody>
                    </table>
                </div>

                <div class="flex justify-center mt-4 space-x-2">
                <button class="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-700 transition duration-300" onclick={
                    let board = board.clone();
                    Callback::from(move |_| {
                        board.dispatch(BoardAction::PlayAsOpponent)
                    })
                }>
                    { "AI Move" }
                </button>
                <button class="px-4 py-2 bg-green-500 text-white rounded hover:bg-green-700 transition duration-300" onclick={
                    let board = board.clone();
                    Callback::from(move |_| {
                        board.dispatch(BoardAction::PrevTurn)
                    })
                }>
                    { "Prev Turn" }
                </button>
            </div>

            </div>
    }
}

#[derive(Clone, Routable, PartialEq)]
enum Route {
    #[at("/")]
    Home,
    #[at("/othello")]
    Othello,
    #[at("/manacala")]
    Mancala,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: Route) -> Html {
    match routes {
        Route::Home => html! { <>
                  <h1>{ "Games" }</h1>
                    <p>{ "Select a game to play" }</p>
                  <ul>
                    <li>
                      <Link<Route> to={Route::Othello}>{ "Othello" }</Link<Route>>
                    </li>
                    <li>
                      <Link<Route> to={Route::Mancala}>{ "Mancala" }</Link<Route>>
                    </li>
                  </ul>
                </>
              },
        Route::Othello => html! { <Othello /> },
        Route::Mancala => html! { <Mancala /> },
        Route::NotFound => html! { <h1>{ "404" }</h1> },
    }
}

#[function_component(App)]
pub fn app() -> Html {
    html! {
        <BrowserRouter>
            <Switch<Route> render={switch} /> // <- must be child of <BrowserRouter>
        </BrowserRouter>
    }
}

fn is_duration_from_now(n: Instant, duration: u64) -> impl Fn() -> bool {
    move || n.elapsed().as_secs() > duration
}

#[cfg(test)]
mod tests {

    use super::*;

    #[test]
    fn test_flip() {
        // Initialize the board with its default state.
        let cases = vec![
            (
                // initial board state
                vec![
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 1, 2, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                ],
                // cell to toggle
                (3, 5),
                // expected board state after toggling
                vec![
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 1, 1, 1, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                ],
                "flip works",
            ),
            (
                // initial board state
                vec![
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 1, 2, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                ],
                // cell to toggle
                (3, 2),
                // expected board state after toggling
                vec![
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 1, 2, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                ],
                "don't flip if illegal move",
            ),
            (
                // initial board state
                vec![
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                ],
                // cell to toggle
                (2, 2),
                // expected board state after toggling
                vec![
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 1, 1, 1, 0, 0, 0],
                    vec![0, 0, 0, 1, 1, 0, 0, 0],
                    vec![0, 0, 0, 2, 1, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                    vec![0, 0, 0, 0, 0, 0, 0, 0],
                ],
                "turn 2",
            ),
            // (
            //     // initial board state
            //     vec![
            //         vec![0, 0, 0, 0, 0, 0, 0, 0],
            //         vec![0, 0, 0, 0, 0, 0, 0, 0],
            //         vec![0, 0, 0, 0, 0, 0, 0, 0],
            //         vec![0, 0, 0, 1, 2, 1, 0, 0],
            //         vec![0, 0, 0, 2, 1, 0, 0, 0],
            //         vec![0, 0, 0, 0, 0, 0, 0, 0],
            //         vec![0, 0, 0, 0, 0, 0, 0, 0],
            //         vec![0, 0, 0, 0, 0, 0, 0, 0],
            //     ],
            //     // expected legal moves
            //     vec![(3, 2), (2, 3), (3, 6), (4, 5), (5, 4)],
            //     "legal moves for player 2",
            // ),
        ];
        cases
            .iter()
            .for_each(|(initial_state, cell, expected_state, msg)| {
                let board = Board {
                    cells: initial_state.clone(),
                    turn: 0,
                    turns: vec![initial_state.clone()],
                };
                board.pretty_print();
                let (board_after_flip, flipped) = board.clone().flip(cell.0, cell.1);
                println!("-------------------");
                board_after_flip.pretty_print();
                println!("-------------------");

                assert_eq!(
                    board_after_flip.cells, *expected_state,
                    "flip failed: {}",
                    msg
                );
            });
    }

    #[test]
    fn test_move_with_best_score() {
        let cases = vec![
            // initial state, expected move, name
            (
                vec![
                    vec![1, 1, 2, 0, 1, 2, 1, 1],
                    vec![1, 1, 1, 1, 1, 1, 1, 1],
                    vec![1, 1, 1, 2, 1, 1, 1, 1],
                    vec![1, 1, 1, 2, 1, 1, 1, 2],
                    vec![2, 2, 2, 2, 2, 2, 2, 1],
                    vec![2, 2, 2, 2, 2, 2, 2, 2],
                    vec![2, 2, 2, 2, 2, 2, 2, 2],
                    vec![2, 1, 2, 0, 2, 2, 2, 2],
                ],
                (7 as usize, 3 as usize),
                "move with best score",
            ),
        ];
        cases
            .iter()
            .for_each(|(initial_state, expected_move, msg)| {
                let mut board = Board {
                    cells: initial_state.clone(),
                    turn: 0,
                    turns: vec![initial_state.clone()],
                };
                board.pretty_print();
                println!("1: {}, 2: {}", board.count_ones(), board.count_twos());

                let move_with_best_score = board.move_with_best_score();
                let r = board.flip(move_with_best_score.0, move_with_best_score.1);

                match r {
                    (b, true) => {
                        b.pretty_print();
                        println!("1: {}, 2: {}", b.count_ones(), b.count_twos());
                        assert!(b.count_ones() > b.count_twos(), "player 1 has more pieces");
                        assert_eq!(move_with_best_score, *expected_move, "{}", msg);
                    }
                    _ => {
                        assert!(false, "illegal move");
                    }
                }
            });
    }
}
